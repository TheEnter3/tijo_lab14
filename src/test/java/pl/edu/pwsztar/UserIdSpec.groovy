package pl.edu.pwsztar

import spock.lang.Specification

class UserIdSpec extends Specification {

    def idList = ['96030223672', '05311663168', 'asdqwezxcrt', '456fgh']


    def "should check if size is correct"() {
        given:
            UserId userId = new UserId(idList.get(0))
            UserId userId1 = new UserId(idList.get(1))
            UserId userId2 = new UserId(idList.get(2))
            UserId userId3 = new UserId(idList.get(3))
        when:
            def answer = [userId.isCorrectSize(),
                          userId1.isCorrectSize(),
                          userId2.isCorrectSize(),
                          userId3.isCorrectSize(),]
        then:
            answer == [true,true,true,false]
    }

    def "should check if sex is valid"(){
        given:
            UserId userId = new UserId(idList.get(0))
            UserId userId1 = new UserId(idList.get(1))
            UserId userId2 = new UserId(idList.get(2))
            UserId userId3 = new UserId(idList.get(3))
        when:
            def answer = [(userId.getSex().toString() == "Optional[MAN]"),
                          userId1.getSex().toString() == "Optional[WOMAN]",
                          userId2.getSex().toString() == "Optional.empty",
                          userId3.getSex().toString() == "Optional.empty"]
        then:
            answer == [true, true, true, true]
    }

    def "should check if id is correct"(){
        given:
            UserId userId = new UserId(idList.get(0))
            UserId userId1 = new UserId(idList.get(1))
            UserId userId2 = new UserId(idList.get(2))
            UserId userId3 = new UserId(idList.get(3))
        when:
            def answer = [userId.isCorrect(),
                          userId1.isCorrect(),
                          userId2.isCorrect(),
                          userId3.isCorrect()]
        then:
            answer == [true,true,false,false]
    }

    def "should return date (dd-mm-yyyy)"(){
        given:
            UserId userId = new UserId(idList.get(0))
            UserId userId1 = new UserId(idList.get(1))
            UserId userId2 = new UserId(idList.get(2))
            UserId userId3 = new UserId(idList.get(3))
        when:
            def answer = [(userId.getDate() == Optional.of('02-03-1996')),
                          userId1.getDate() == Optional.of('16-11-2005'),
                          userId2.getDate() == Optional.empty(),
                          userId3.getDate() == Optional.empty()]
        then:
            answer == [true, true, true, true]
    }
}
